package com.cogso.insowa.repo.remote

import com.dalvendra.assignment.repo.remote.ResponseCards
import com.google.gson.JsonElement
import retrofit2.Call
import retrofit2.http.*


interface InsowaApi {
    @GET("api/")
    fun getCards(@Query("results") results:Int) : Call<ResponseCards>

}