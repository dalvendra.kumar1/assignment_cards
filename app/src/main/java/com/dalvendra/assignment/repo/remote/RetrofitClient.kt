package com.cogso.insowa.repo.remote

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object RetrofitClient {
    private const val BASE_URL = "https://randomuser.me/"
    private var loggingInterceptor=HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)
    private val okHttpClient = OkHttpClient.Builder()
            .addInterceptor { chain ->
                val original = chain.request()
                val requestBuilder = original.newBuilder()
                        .addHeader("Content-Type", "application/json")
                        .method(original.method, original.body)

                val request = requestBuilder.build()
                chain.proceed(request)
            }
            .readTimeout(30,TimeUnit.SECONDS)
            .addInterceptor(loggingInterceptor)
            .build()

    val instance: InsowaApi by lazy{
        val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build()
        retrofit.create(InsowaApi::class.java)
    }
}