package com.dalvendra.assignment.models

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions

data class Cards(var gender:String,
                 var name:Name,
                 var location: Location,
                 var email:String,
                 var picture:Picture,
                 var cell:String,
                 var dob: Dob) {

    data class Name(var title: String,
                    var first:String,
                    var last:String)
    data class Picture(var large: String,
                       var medium:String,
                       var thumbnail:String)
    data class Dob(var date: String,
                   var age:Int)
    fun  nameAgeField():String {
        return name.first+"("+dob.age+")"
    }
    fun  addressField():String {
        return ""+location.street.number+","+location.street.name+","+location.city+","+location.state+","+location.country
    }

    companion object{
        @BindingAdapter("loadPicture")
        @JvmStatic
        fun loadPicture(view: ImageView, url: String){
            Glide.with(view.context)
                .load(url)
                .apply(RequestOptions().circleCrop())
                .into(view)
        }
    }

}