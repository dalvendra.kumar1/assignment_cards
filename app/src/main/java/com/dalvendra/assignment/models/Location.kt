package com.dalvendra.assignment.models

class Location (var street:Street,
                var city:String,
                var state:String,
                var country:String,
                var postcode:String
)
data class Street(var number:Int,
                  var name:String)