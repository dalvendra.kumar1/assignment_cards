package com.dalvendra.assignment.ui.cards.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.dalvendra.assignment.R
import com.dalvendra.assignment.databinding.ItemCardsBinding
import com.dalvendra.assignment.models.Cards

class CardsAdapter : PagedListAdapter<Cards, CardsAdapter.CustomerHolder>(
    productDiffCallBack()
)  {
    class CustomerHolder(var binding: ItemCardsBinding) :
        RecyclerView.ViewHolder(binding.root)

    class productDiffCallBack : DiffUtil.ItemCallback<Cards>(){
        override fun areItemsTheSame(oldItem: Cards, newItem: Cards): Boolean {
            return oldItem.cell==newItem.cell
        }
        override fun areContentsTheSame(oldItem: Cards, newItem: Cards): Boolean {
            return oldItem.equals(newItem)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomerHolder {
        val binding=
            DataBindingUtil.inflate<ItemCardsBinding>(
                LayoutInflater.from(parent.context),
                R.layout.item_cards, parent, false
            )
        return CustomerHolder(
            binding
        )
    }

    override fun onBindViewHolder(holder: CustomerHolder, position: Int) {
        val cards=getItem(position)
        holder.binding.card=cards
    }
}