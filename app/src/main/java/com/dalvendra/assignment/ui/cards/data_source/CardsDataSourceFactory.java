package com.dalvendra.assignment.ui.cards.data_source;

import androidx.lifecycle.MutableLiveData;
import androidx.paging.DataSource;
import androidx.paging.PageKeyedDataSource;
import com.dalvendra.assignment.data_source.CardsDataSource;
import com.dalvendra.assignment.models.Cards;


public class CardsDataSourceFactory extends DataSource.Factory {
    public CardsDataSourceFactory(MutableLiveData<Boolean> noData) {
        this.noData=noData;
    }
    private MutableLiveData<Boolean> noData;
    private CardsDataSource dataSource;
    // creating the mutable live data
    private MutableLiveData<PageKeyedDataSource<Integer, Cards>> itemLiveDataSource = new MutableLiveData<>();

    @Override
    public DataSource<Integer, Cards> create() {
        //getting our data source object
        dataSource = new CardsDataSource(noData);
        //posting the data source to get the values
        itemLiveDataSource.postValue(dataSource);
        //returning the data source
        return dataSource;
    }
    public MutableLiveData<PageKeyedDataSource<Integer, Cards>> getItemLiveDataSource() {
        return itemLiveDataSource;
    }
}
