package com.dalvendra.assignment.ui.cards.viewModels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.paging.LivePagedListBuilder
import androidx.paging.PageKeyedDataSource
import androidx.paging.PagedList
import com.dalvendra.assignment.models.Cards
import com.dalvendra.assignment.ui.cards.data_source.CardsDataSourceFactory

class CardsViewModel :ViewModel() {
    var noData = MutableLiveData<Boolean>()

    var listCards : LiveData<PagedList<Cards>>
    var customerDataSourceFactory: CardsDataSourceFactory
    private  var pageKeyedDataSource: MutableLiveData<PageKeyedDataSource<Int, Cards>>
    init {
        customerDataSourceFactory =
            CardsDataSourceFactory(
                noData
            )
        pageKeyedDataSource =customerDataSourceFactory.itemLiveDataSource
        val pagedListConfig= (PagedList.Config.Builder())
            .setEnablePlaceholders(true)
            .setPageSize(20).build()
        listCards = LivePagedListBuilder(customerDataSourceFactory, pagedListConfig).build() as LiveData<PagedList<Cards>>
    }
}