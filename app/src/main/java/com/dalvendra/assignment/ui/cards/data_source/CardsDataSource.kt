package com.dalvendra.assignment.data_source
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import com.cogso.insowa.repo.remote.RetrofitClient
import com.dalvendra.assignment.models.Cards
import com.dalvendra.assignment.repo.remote.ResponseCards
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CardsDataSource(var noData: MutableLiveData<Boolean>) : PageKeyedDataSource<Int?, Cards?>() {
    override fun loadInitial(params: LoadInitialParams<Int?>, callback: LoadInitialCallback<Int?, Cards?>) {
        Log.d(TAG, "loadInitial called")
        RetrofitClient.instance.getCards(10)
            .enqueue(object : Callback<ResponseCards> {
                override fun onResponse(
                    call: Call<ResponseCards>,
                    response: Response<ResponseCards>
                ) {
                    response.body()?.let {
                        if (it.results.size==0)
                            noData.value=true
                        else
                            noData.value=false
                        callback.onResult(response.body()?.results!! ,null,(2))
                    }
                }

                override fun onFailure(call: Call<ResponseCards>, t: Throwable) {
                    Log.i(TAG,t.localizedMessage)
                }
            })
    }

    override fun loadBefore(params: LoadParams<Int?>, callback: LoadCallback<Int?, Cards?>) {}

    override fun loadAfter(params: LoadParams<Int?>, callback: LoadCallback<Int?, Cards?>) {
        Log.d(TAG, "loadAfter called key "+params.key+" requestedLoadSize  "+params.requestedLoadSize)
        RetrofitClient.instance.getCards(10)
            .enqueue(object : Callback<ResponseCards> {
                override fun onResponse(
                    call: Call<ResponseCards>,
                    response: Response<ResponseCards>
                ) {
                    Log.d(TAG,response.body().toString())
                    response.body()?.let {
                        var key = if (it.info.results<10 ) null else params.key + 1
                        callback.onResult(it.results ,key)
                    }
                }

                override fun onFailure(call: Call<ResponseCards>, t: Throwable) {
                    Log.i(TAG,t.localizedMessage)
                }
            })
    }
    companion object {
        private val TAG = CardsDataSource::class.java.simpleName
    }


}