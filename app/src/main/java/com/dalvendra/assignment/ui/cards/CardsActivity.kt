package com.dalvendra.assignment.ui.cards

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.paging.PagedList
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.dalvendra.assignment.R
import com.dalvendra.assignment.databinding.ActivityCardsBinding
import com.dalvendra.assignment.models.Cards
import com.dalvendra.assignment.ui.cards.adapter.CardsAdapter
import com.dalvendra.assignment.ui.cards.viewModels.CardsViewModel

class CardsActivity : AppCompatActivity() {
    lateinit var binding:ActivityCardsBinding
    lateinit var viewModel:CardsViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding=DataBindingUtil.setContentView(this,R.layout.activity_cards)

        viewModel= ViewModelProviders.of(this).get(CardsViewModel::class.java)

        var adapter= CardsAdapter()
        binding.recyclerView.layoutManager= LinearLayoutManager(this)
        binding.recyclerView.itemAnimator= DefaultItemAnimator()
        binding.recyclerView.adapter=adapter

        viewModel.listCards.observe(this,object : Observer<PagedList<Cards>> {
            override fun onChanged(t: PagedList<Cards>?) {
                adapter.submitList(t)
            }
        })
    }
}